class Persona
    attr_accessor :dni,:apellido,:nombre
    def initialize(dni,apellido,nombre)
        @dni = dni
        @apellido = apellido
        @nombre = nombre
    end
end

class Tutor < Persona
    attr_accessor :parentesco
    def initialize(dni,apellido,nombre,parentesco)
        super(dni,apellido,nombre)
          @parentesco=parentesco
    end
end

class Alumno < Persona
    attr_accessor :edad,:genero,:tutorArray
    def initialize(dni,apellido,nombre,edad,genero)
          @edad=edad
          @genero=genero
          @tutorArray=[]
    end
    def calcularCS
    end
    def calcularRE
    end
    def ingresarTutor(tutor)
        tutorArray.push(tutor)
    end
end

class AlumnoColegioNacional < Alumno
    attr_accessor :zona ,:promedioPonderado
    def initialize(dni,apellido,nombre,edad,genero,zona,promedioPonderado)
        super(dni,apellido,nombre,edad,genero)
        @zona=zona
        @promedioPonderado=promedioPonderado
    end
    def ObternerZona
            puntaje = 0
        if zona == "Rural"
            puntaje = 100
        elsif zona == "Urbana"
            puntaje =  80
        end
        return puntaje
     end
    def CalcularPromedioPonderado
        puntaje=0
        if promedioPonderado>=19
            puntaje=100
        elsif(promedioPonderado>=18 and promedioPonderado<19)
            puntaje=80
        elsif(promedioPonderado>=16 and promedioPonderado<18)
            puntaje=60
        elsif(promedioPonderado>=14 and promedioPonderado<16)
            puntaje=40
        elsif(promedioPonderado>=11 and promedioPonderado<14)
            puntaje=20
        elsif promedioPonderado<11
            puntaje=0
        end
        return puntaje
    end
end

class AlumnoColegioParticular < Alumno
    attr_accessor :monto ,:puesto
    def initialize(dni,apellido,nombre,edad,genero,monto,puesto)
        super(dni,apellido,nombre,edad,genero)
        @monto=monto
        @puesto=puesto
    end
    def CalcularPension
          punto =0
        if monto>=200
            punto=90
        elsif(monto>200 and monto<=400)
            punto=70
        elsif(monto>400 and monto<=600)
            punto=50
        elsif pension>600
            punto=40
        end
         return punto
    end
    def CalcularPuesto
         punto=0
        if(puesto=="1"and puesto=="2" and puesto=="3")
            punto=100
        elsif(puesto=="4" and puesto=="5")
            punto=80
        elsif (puesto=="10" and puesto=="6")
            punto=60
        elsif(puesto=="20" and puesto=="11")
            punto=40
        else 
            punto=0
        end
        return punto
    end
end

class Postulante 
    attr_accessor :codigoPostulante,:alumnos
    def initialize(codigoPostulante)
        @codigoPostulante = codigoPostulante
        @alumnos = []
    end
    def registrarAlumnos(alumno)
        alumnos.push(alumno)
    end
    def listarAlumno
         listaAge = []
        for alumno in alumnos
            listaAge.push(" El: #{alumno.nombre}")
        end
    end
    def calcularPuntajeEC
    end
    def calcularPuntajeFinal
    end
end

class Evaluaccion
    attr_accessor :codigoEvaluacion,:tipoEvaluacion, :alumnosArray, :preguntaArray
    def initialize(codigoEvaluacion,tipoEvaluacion)
        @codigoEvaluacion = codigoEvaluacion
        @tipoEvaluacion = tipoEvaluacion
        @alumnosArray = []
        @preguntaArray = []
    end
    def calcularPuntaje
        puntaje = 0
    end
    def ingresarAlumnos(alumnos)
        alumnosArray.push(alumnos)
    end
    def ingresarPreguntas(preguntas)
        suma=0
    if tipoEvaluacion == "10"
        suma = 2
    elsif tipoEvaluacion == "20"
        suma = 1
    end
        return suma
    end
end

class Pregunta 
    attr_accessor :descripcion, :preguntaArray
    def initialize(descripcion)
        @descripcion = descripcion
        @preguntaArray = []
    end
    def registrarRealizacionPregunta(realizacion)
        preguntaArray.push(realizacion)
    end
    def listarRespuestasOk
        for r in preguntaArray
           if (r.correcto =false)
            return r
           end 
        end
    end
end

class Respuesta < Pregunta
    attr_accessor :alternativa,:estado
    def initialize(descripcion,alternativa,estado)
        super(descripcion)
        @alternativa = alternativa
        @estado = estado
    end
    def validarRespuesta
        mensaje = 'correcto'
    end
end



per1 = Persona.new("856478950","Rojas","Victor")
alum1  = Alumno.new("856478950","Rojas","Victor",21,"M")
alumNaci = AlumnoColegioNacional.new("856478950","Rojas","Victor",21,"M","Rural",80)
alumPar = AlumnoColegioParticular.new("856478950","Rojas","Victor",21,"M",300,80)
post = Postulante.new("u20202020200")

post.registrarAlumnos(alum1)

puts "#{post.listarAlumno}"

realPreg1 = Respuesta.new("Lima","A",false)
realPreg2 = Respuesta.new("Arequipa","b",true)
realPreg3 = Respuesta.new("Loreto","c",false)

realPreg4 = Respuesta.new("50","a",true)
realPreg5 = Respuesta.new("3","b",false)
realPreg6 = Respuesta.new("4","c",false)
pregu1 = Pregunta.new("1 pregunta : Capital de Peru")
pregu1.registrarRealizacionPregunta(realPreg1)
pregu1.registrarRealizacionPregunta(realPreg2)
pregu1.registrarRealizacionPregunta(realPreg3)

pregu2 = Pregunta.new("2 pregunta ; 2 + 2")
pregu2.registrarRealizacionPregunta(realPreg4)
pregu2.registrarRealizacionPregunta(realPreg5)
pregu2.registrarRealizacionPregunta(realPreg6)

eva = Evaluaccion.new("u001","2 preguntas")

eva.ingresarPreguntas(pregu1)
eva.ingresarPreguntas(pregu2)

puts "#{eva.codigoEvaluacion} || #{eva.tipoEvaluacion} || #{eva.calcularPuntaje}"
puts 
puts "#{realPreg2.validarRespuesta}"
